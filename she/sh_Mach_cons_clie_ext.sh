#!/usr/bin/sh
#***************************************************************************
# Nombre      : sh_Mach_cons_clie_ext.sh
# Ruta        : /MIGCON/she/
# Autor       : Felipe Pousa (Ada Ltda.) - Claudio Bello (Ing. Software. Bci)
# Fecha       : 22/12/2021
# Descripcion : Carga informacion de consentimiento de cliente desde Mach
# Parametros  : no aplica
# Ejemplo de Ejecucion  : sh sh_Mach_cons_clie_ext.sh
#***************************************************************************
# Mantencion  :
# Empresa     :
# Fecha       :
# Descripcion :
#***************************************************************************


#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_STT=0
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	AHORA=`date +'%Y%m%d%H%M'`

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/tmp/${BSE_PGM}_${INF_PID}.log
export archLog


#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
unifica_archivos_log()
{
	printlog 1 "Unificando en el log archivos para su analisis"

	if [ -f ${DirTmp}/${INF_PID}*.tmp ]
	then
		for FILE in ${DirTmp}/${INF_PID}*.tmp
		do
			printlog 2 "-------------------------------------------------------------"
			printlog 2 "Archivo: $FILE"
			printlog 2 "Contenido:"
			printlog 2 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			cat $FILE| grep -i -v "\.logon" >> ${archLog}
			printlog 2 "--------------------- FIN DE ARCHIVO ------------------------"
			eliminar_archivo $FILE
		done
	fi
}

printlogfinal()
{	
	printlog 2 "valida si log existe"
	if [ "x${archLogFnl}" = "x" ]
	then
		#Si el nombre del log real no se ha construido debido a que no se obtuvo el parametro de
		#fecha para la extension
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			 "correctos."
		echo "Favor de ver log de paso ${archLog}"
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			"correctos." >> ${archLog}
		echo "Favor de ver log de paso ${archLog}" >> ${archLog}
		return
	fi
	
	printlog 2 "valida si log tiene datos"
	if [ -s ${archLogFnl} ]
	then
		# Si existe el log final, quiere decir que ya existe una ejecucion del proceso para
		# la misma fecha, por lo cual, se debe concatenar el resultado de esta ejecucion
		# con el resultado de la ejecucion anterior en el mismo archivo.
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" 
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "######### --->                  INICIO EJECUCION `date +%d/%m/%y`                  "\
			"<--- #############" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "##### ##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
		echo "" ${archLogFnl}
		cat ${archLog} >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "##### Fin de la ejecucion  `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"\
			>> ${archLogFnl}
		rm -f ${archLog}
	else
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
	fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1"
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi

	#verifica archivo distinto de vacio
	printlog 2 "verifica archivo distinto de vacio"
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de Profile..."
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}

	
Valida_num_param()
{
	# $1 Cantidad de parametros a validar
	# $2 Cantidad de parametros correctas
	
	printlog 2 "Funcion Valida_num_param - Parametro 1: $1"
	printlog 2 "Funcion Valida_num_param - Parametro 2: $2"
	
	if [ $1 -ne $2 ] 
	then
		printlog 2 "Funcion Valida_num_param : ERROR - No se requiere de parametros"
		Syntax
		SALIR 2  " FALLIDO - No se requiere de parametros"
	fi
}


valida_definicion_variables () {
if [ "x$1" = "x" ]
then
	printlog 1 "Funcion valida_definicion_variables - Error, no hay variables a validar"
	SALIR 2  " FALLIDO - No se han definido variables"
fi

for V in $1
do
	if [ "x$V" = "x" ]
	then
		printlog 1 "Funcion valida_definicion_variables - ERROR: La variable de ambiente $V no esta definida"
		SALIR 2 " FALLIDO - No se han definido la variable $V"
	else
		printlog 2 "Funcion valida_definicion_variables - La variable de ambiente $V se encuentra definida"
	fi
done
}

# Elimina el archivo validando previamente si existe
eliminar_archivo () 
{
	if [ -f $1 ]
	then
		rm -f $1
		printlog 2 "Se elimina archivo $1"
	else
		printlog 2 "Archivo $1 no puede ser eliminado. Motivo:"
		if [ -d $1 ]
		then
			printlog 2 "Archivo $1 es un directorio."
		else
			printlog 2 "Archivo $1 no existe"
		fi
	fi
}

# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t sh ${INF_PGM}"
	printlog 1 ""
	printlog 1 "Ejemplo:"
	printlog 1 "sh ${INF_PGM}"

}



#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de consults sql
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/Cons_Cli_Profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile


#valida definicion de variables de ambiente
valida_definicion_variables "${VARIABLE_AMBIENTE}"


#valida parametro de entrada
Valida_num_param $# $Cant_Min_Param


##############################################################
################### INICIO EJECUCION SHELL ###################
##############################################################

# Si no se ha asignado un archivo LOG como parametro, se define uno por defecto.
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${AHORA}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

printlog 2 "Valida si debe sobreescribir el log"
if [ "x$INF_SOBRELOG"="xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi



# -------------------- FUNCIONES EXCLUSIVAS SQL SERVER --------------------------

Sb_Realiza_Conexion () 
{
	printlog 1 "Funcion Sb_Realiza_Conexion - El sistema al obtener es $1"
	P_USUARIO=""
	printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_USUARIO: $P_USUARIO"
	P_CLAVE=""
	printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_CLAVE: $P_CLAVE"
	UYPRSP=$(UyPuser $1)
	if [[ $? != 0 ]]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Se cancelo la ejecucion de UyPuser"
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : SISTEMA = $1 "
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : RETORNO = ${UYPRSP}"
		P_STAT="ERR"
		printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_STAT: $P_STAT"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	else
		uypcnt=0
		for uyprsp in ${UYPRSP}
		do
			printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: uypcnt: $uypcnt"
			uypDa[$uypcnt]=$uyprsp
			(( uypcnt=$uypcnt+1))
		done
		if [ "${uypDa[0]}" = "ERROR" ] ; then
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : No se pudo obtener la clave del sistema $1"
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : RETORNO = ${uypDa[1]}"
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : se aborta ejecucion"
			SALIR 2 " FALLIDO - Error al obtener UyPuser."
		else
			P_USUARIO=${uypDa[0]}
			P_CLAVE=${uypDa[1]}
			printlog 1 "Funcion Sb_Realiza_Conexion - Se obtiene la informacion del sistema $1"
			printlog 2 "Funcion Sb_Realiza_Conexion - Usuario obtenido : ${P_USUARIO}"
			printlog 2 "Funcion Sb_Realiza_Conexion - Clave no registrada, pero obtenida"
		fi
fi
	printlog 2 "Funcion Sb_Realiza_Conexion - Validacion de nombre de usuario"
	if [ x"$P_USUARIO" = "x" ]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Usuario obtenido es vacio [Sistema = $1]"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	fi
	if [ x"$P_CLAVE" = "x" ]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Clave obtenida es vacia [Sistema = $1]"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	fi
	
	export P_USUARIO
	export P_CLAVE

	
	printlog 1 "Funcion Sb_Realiza_Conexion - Obtenidos los datos correctamente."
}


Sb_Elimina_Registros()
{
	printlog 1 "Funcion Sb_Elimina_Registros - Se ingresa a la funcion"
	printlog 2 "Funcion Sb_Elimina_Registros - Parmetro 1 - Base de Datos: $1"
	printlog 2 "Funcion Sb_Elimina_Registros - Parmetro 2 - Tabla: $2"

#ELIMINA EN CASO DE REPROCESO


fisql -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} << eof >> $DirTmp/${INF_PID}_SqlServer.tmp
use $1 
go
delete from dbo.$2
go
eof
	RETORNO=$?
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO != 0 ]] then 
		printlog 1 "Funcion Sb_Elimina_Registros - ERROR : El borrado de registros de la tabla $2 se cancelo"
		SALIR 2 "FALLIDO - El borrado de registros de la tabla $2 se cancelo"
	elif [ -f $DirTmp/${INF_PID}_SqlServer.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirTmp/${INF_PID}_SqlServer.tmp`
		if [[ "${verE}" != "0" ]]; then
			printlog 2 "Funcion Sb_Elimina_Registros - ERROR : El borrado de registros de la tabla $2 retorno una excepcion"
			SALIR 2 "FALLIDO - El borrado de registros de la tabla $2 retorno una excepcion"
		fi
	fi
	
	printlog 1 "Funcion Sb_Elimina_Registros - Los registros se han eliminado exitosamente"
	printlog 2 "Funcion Sb_Elimina_Registros - Se respalda Log de la eliminacion"
	cat $DirTmp/${INF_PID}_SqlServer.tmp >> ${archLog}
	eliminar_archivo $DirTmp/${INF_PID}_SqlServer.tmp
	
}



Sb_Extrae_Registros()
{
	printlog 1 "Funcion Sb_Extrae_Registros - Se ingresa a la funcion"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 1 - Base de Datos: $1"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 2 - Campo: $2"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 3 - Tabla: $3"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 4 - Condicion: \"$4\""
	
#ELIMINA EN CASO DE REPROCESO


fisql -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} << eof >> $DirTmp/${INF_PID}_SqlServer_dato.tmp
use $1 
go
select $2 from dbo.$3 where ori_nombre = '$4'
go
eof
	RETORNO=$?
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO != 0 ]] then 
		printlog 1 "Funcion Sb_Extrae_Registros - ERROR : La extraccion de registros de la tabla $3 se cancelo"
		SALIR 2 "FALLIDO - La extraccion de registros de la tabla $3 se cancelo"
	elif [ -f $DirTmp/${INF_PID}_SqlServer_dato.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirTmp/${INF_PID}_SqlServer_dato.tmp`
		if [[ "${verE}" != "0" ]]; then
			printlog 2 "Funcion Sb_Extrae_Registros - ERROR : La extraccion de registros de la tabla $3 retorno una excepcion"
			SALIR 2 "FALLIDO - La extraccion de registros de la tabla $3 retorno una excepcion"
		fi
	fi
	
	sin_valor=`sed -n 1p $DirTmp/${INF_PID}_SqlServer_dato.tmp | awk 'BEGIN {FS=";"}; {print $1}'`
	export sin_valor
	
	if [[ $sin_valor = '(0 rows affected)' ]] then 
		printlog 1 "Funcion Sb_Extrae_Registros - ERROR : No existen origen id en la tabla $3, para MACH "
		SALIR 2 "SE CANCELA - No existe origen id en la tabla $3, para MACH"
	fi
	
	printlog 1 "Funcion Sb_Extrae_Registros - Los registros se han extraido exitosamente"
	printlog 2 "Funcion Sb_Extrae_Registros - Se respalda Log de la extraccion"
	
	#Elimina espacios y tabulaciones al final de una linea
	sed 's/[ \t]*$//' $DirTmp/${INF_PID}_SqlServer_dato.tmp >> $DirTmp/${INF_PID}_SqlServer_dato_sin_espacio.tmp

	#registro obtenido de 
	valor=`sed -n 3p $DirTmp/${INF_PID}_SqlServer_dato_sin_espacio.tmp | awk 'BEGIN {FS=";"}; {print $1}'`
	export valor

	cat $DirTmp/${INF_PID}_SqlServer_dato.tmp >> ${archLog}
	eliminar_archivo $DirTmp/${INF_PID}_SqlServer_dato.tmp
	eliminar_archivo $DirTmp/${INF_PID}_SqlServer_dato_sin_espacio.tmp
	
}



Sb_Realiza_BcpIn ()
{ 
# Paratmeros: $1 = BD	 $2 = nombre_tabla	$3 = nombre_archivo a cargar $4 = separador $5 = nombre archivo original

printlog 1 "Funcion Sb_Realiza_BcpIn - Se ingresa a la funcion"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 1 - Base de Datos: $1"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 2 - Tabla: $2"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 3 - Ruta/nombre_archivo a carga: $DirTmp/$3"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 4 - Separador: $4"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 5 - Nombre del archivo original: $5"

printlog 2 "Funcion Sb_Realiza_BcpIn - Variable externa - Motor: ${SbMotor}"
printlog 2 "Funcion Sb_Realiza_BcpIn - Variable externa - Usuario: ${P_USUARIO}"

cd $DirTmp
if [ -s $3 ]
	 then
	numreg=$(wc -l $3| awk '{ print $1 }')
	printlog 1 "Funcion Sb_Realiza_BcpIn - Registros a Cargar en tabla $2: $numreg"  

	freebcp $1..$2 in $3 -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} -c -b1000 -t $4 -e $DirTmp/${INF_PID}_SqlServer_bcp_err.tmp >> $DirFtpIn/${INF_PID}_SqlServer.tmp
	RETORNO=$?
	
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO -ne 0 ]]
	then 
		printlog 1 "Funcion Sb_Realiza_BcpIn - ERROR : El BCP IN a la tabla $2 se cancelo"
		SALIR 2 "Funcion Sb_Realiza_BcpIn - ERROR : El BCP IN a la tabla $2 se cancelo"
	elif [ -f $DirFtpIn/${INF_PID}_SqlServer.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirFtpIn/${INF_PID}_SqlServer.tmp`

		if [[ "${verE}" != "0" ]]; then
			printlog 1 "Funcion Sb_Realiza_BcpIn - ERROR : BCP IN a la tabla $2 retorno una excepcion"
			SALIR 2 "Funcion Sb_Realiza_BcpIn - ERROR : BCP IN a la tabla $2 retorno una excepcion"
		fi
	fi
	#Mueve archivo original a directorio Out
	mv $DirFtpIn/$5 $DirFtpOut/$5
	
	printlog 1 "Funcion Sb_Realiza_BcpIn - El archivo $3 se ha cargado correctamente"
	printlog 2 "Funcion Sb_Realiza_BcpIn - El archivo $3 se ha cargado correctamente, Se respalda Log de carga"
	cat $DirFtpIn/${INF_PID}_SqlServer.tmp >> ${archLog}
	eliminar_archivo $DirFtpIn/${INF_PID}_SqlServer.tmp

	
	
fi
}


Sb_Valida_Cant_arch ()
{
	# $1 Cantidad de archivos detectados
	# $2 Cantidad de archivos permitidos
	# $3 Nombre del archivo a cargar
	
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $1 -gt $2 ]] then 
		printlog 1 "Funcion Sb_Valida_Cant_arch - ERROR : Existe $cantidad archivos $3%.csv con el mismo nombre, se cancelo"
		SALIR 2 "FALLIDO - Existe $cantidad archivos $3%.csv con el mismo nombre, se cancelo"
		
			printlog 2 "Funcion Sb_Valida_Cant_arch - ERROR : Existe $cantidad archivos $3%.csv con el mismo nombre, se cancelo"
			SALIR 2 "FALLIDO - Existe $cantidad archivos $3%.csv con el mismo nombre, se cancelo"
	fi
}	



Sb_Valida_Cant_Columnas ()
{

cat ${1} | awk ' length($0)> 0' | awk -F';' 'NF!=3' > ${DirTmp}/val_colum.tmp

if [ -s ${DirTmp}/val_colum.tmp ]; then
		printlog 1 "Funcion Sb_Valida_Cant_Columnas - ERROR : El archivo ${1} tiene lineas con formato incorrecto de columnas, se cancelo"
		printlog 2 "Funcion Sb_Valida_Cant_Columnas - ERROR : El archivo ${1} tiene lineas con formato incorrecto de columnas, se cancelo"
		
		eliminar_archivo ${DirTmp}/val_colum.tmp
		
		SALIR 2 "FALLIDO - El archivo ${1} tiene lineas con formato incorrecto de columnas, se cancelo"
		
		
fi

}


cd $DirFtpIn

func_valida_archivo $DirFtpIn/$Nom_ach_Mach*

#muestra todos los archivos que empiecen con Respaldo_Mach
find $Nom_ach_Mach* > ${DirTmp}/lista_Mach.tmp

#cuenta la cantidad de archivos que empiecen con Respaldo_Mach
grep -c $Nom_ach_Mach ${DirTmp}/lista_Mach.tmp > ${DirTmp}/cant_Respaldo_Mach.tmp

#garda en variable la cantidad del mismo archivo
cantidad=`awk 'BEGIN {FS=";"}; {print substr($1,1,5)}' ${DirTmp}/cant_Respaldo_Mach.tmp `
eliminar_archivo ${DirTmp}/cant_Respaldo_Mach.tmp

#valida cantidad de archivos de entrada
Sb_Valida_Cant_arch $cantidad $Cant_Arch_Mach $DirFtpIn/$Nom_ach_Mach

#extrae nombre de archivo a cargar sin directorio
Nom_ach_ent_Mach_02=`awk '{print $0}' ${DirTmp}/lista_Mach.tmp `

#extrae nombre de archivo a cargar con directorio
Nom_ach_ent_Mach=$DirFtpIn/$Nom_ach_ent_Mach_02

eliminar_archivo ${DirTmp}/lista_Mach.tmp

#valida archivo de entrada, de origen Mach
func_valida_archivo $Nom_ach_ent_Mach
Sb_Valida_Cant_Columnas $Nom_ach_ent_Mach


#variable archivo temporal sin cabera
tmp_ach_ent_Mach=${DirTmp}/tmp_ach_ent_Mach.tmp

#quita cabecera
sed '1,1d' $Nom_ach_ent_Mach > $tmp_ach_ent_Mach

################################
#FORMATEO DE ARCHIVO
################################

#Separa columna 1 en rut y dv
awk 'BEGIN {FS=";"}; { if (length($1) == 9) {print substr($1,1,8) ";" substr($1,9,1) ";" $2 ";" $3 ";" } \
else  {print substr($1,1,7)";"substr($1,8,1) ";" $2 ";" $3 ";" }}' $tmp_ach_ent_Mach > ${DirTmp}/RutDv_ConsMach.tmp

#Elimina archivos de paso
eliminar_archivo $tmp_ach_ent_Mach

#reemplaza Z por vacio
sed -e 's/[Z]//g' ${DirTmp}/RutDv_ConsMach.tmp > ${DirTmp}/FormatoFechaMach1.tmp
eliminar_archivo ${DirTmp}/RutDv_ConsMach.tmp

#reemplaza T por Espacio
sed -e 's/[T]/ /g' ${DirTmp}/FormatoFechaMach1.tmp > ${DirTmp}/FormatoFechaMach2.tmp
eliminar_archivo ${DirTmp}/FormatoFechaMach1.tmp

#Obtiene datos de conexion
Sb_Realiza_Conexion $SbUyPSist

#Obtiene valor de origen_id para Mach
Sb_Extrae_Registros $SbBD $campo $tabla "${condicion2}"

#Genera archivo final con formato definido en reglas de negocio
awk -v EnrBci="$valor" -v Pol_id="$Politica_id_Mach" -v Cnl_Bci="$Canal_Mach" -v Cons="$consentimiento_Mach" \
'BEGIN {FS=";"}; {print $1 ";" $2 ";" EnrBci ";" $3 ";" Pol_id ";" $4 ";" Cnl_Bci ";" Cons}' \
 ${DirTmp}/FormatoFechaMach2.tmp > ${DirTmp}/ConsentMach_Final.tmp
 
#Elimina Archivo de paso
eliminar_archivo ${DirTmp}/FormatoFechaMach2.tmp

#Elimina registros para realizar carga
Sb_Elimina_Registros $SbBD $tbl

#Realiza carga en la tabla consentimiento_de_paso
Sb_Realiza_BcpIn $SbBD $tbl "ConsentMach_Final.tmp" $separador $Nom_ach_ent_Mach_02

#Elimina archivos de paso
eliminar_archivo ${DirTmp}/ConsentMach_Final.tmp
eliminar_archivo ${DirTmp}/val_colum.tmp

printlog 1 "Ejecucion Correcta"
SALIR 0  "CORRECTO"
