#!/usr/bin/sh
#***************************************************************************
# Nombre      : sh_cons_clie_ext.sh
# Ruta        : /MIGCON/she/
# Autor       : Francisco Valenzuela (Ada Ltda.) - Claudio Bello (Ing. Software. Bci)
# Fecha       : 08/11/2021
# Descripcion : Extrae informacion de consentimiento de clientes desde bd Postgress
# Parametros  : yyyymmdd
# Ejemplo de Ejecucion  : sh sh_cons_clie_ext.sh 2021108 
#***************************************************************************
# Mantencion  :
# Empresa     :
# Fecha       :
# Descripcion :
#***************************************************************************


#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_STT=0
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	AHORA=`date +'%Y%m%d%H%M'`

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/tmp/${BSE_PGM}_${INF_PID}.log
export archLog


#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
unifica_archivos_log()
{
	printlog 1 "Unificando en el log archivos para su analisis"

	if [ -f ${DirTmp}/${INF_PID}*.tmp ]
	then
		for FILE in ${DirTmp}/${INF_PID}*.tmp
		do
			printlog 2 "-------------------------------------------------------------"
			printlog 2 "Archivo: $FILE"
			printlog 2 "Contenido:"
			printlog 2 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			cat $FILE| grep -i -v "\.logon" >> ${archLog}
			printlog 2 "--------------------- FIN DE ARCHIVO ------------------------"
			eliminar_archivo $FILE
		done
	fi
}

printlogfinal()
{	
	printlog 2 "valida si log existe"
	if [ "x${archLogFnl}" = "x" ]
	then
		#Si el nombre del log real no se ha construido debido a que no se obtuvo el parametro de
		#fecha para la extension
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			 "correctos."
		echo "Favor de ver log de paso ${archLog}"
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			"correctos." >> ${archLog}
		echo "Favor de ver log de paso ${archLog}" >> ${archLog}
		return
	fi
	
	printlog 2 "valida si log tiene datos"
	if [ -s ${archLogFnl} ]
	then
		# Si existe el log final, quiere decir que ya existe una ejecucion del proceso para
		# la misma fecha, por lo cual, se debe concatenar el resultado de esta ejecucion
		# con el resultado de la ejecucion anterior en el mismo archivo.
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" 
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "######### --->                  INICIO EJECUCION `date +%d/%m/%y`                  "\
			"<--- #############" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "##### ##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
		echo "" ${archLogFnl}
		cat ${archLog} >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "##### Fin de la ejecucion  `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"\
			>> ${archLogFnl}
		rm -f ${archLog}
	else
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
	fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1"
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi

	#verifica archivo distinto de vacio
	printlog 2 "verifica archivo distinto de vacio"
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de Profile..."
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}


#Funcion valida Fecha Proceso
validafechaentrada() 
{
	#$1 Fecha a validar
	
	# Validacion del largo de la cadena
	lenstr=$(expr length $1)
	if [ ! $lenstr -eq 8 ]
	then
		printlog 1 "Funcion validafechaentrada : ERROR - La cadena de la fecha debe tener 8 caracteres"
		SALIR 2 " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	#Validacion del anno
	anno=$(expr substr $1 1 4)
	if [ $anno -le 1899 ]
	then
 		printlog 1 "Funcion validafechaentrada : ERROR - El anno debe ser mayor o igual a 1899"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	# Validacion del mes
	mes=$(expr substr $1 5 2)
	if [ $mes -lt 1 ]
	then
		printlog 1 "Funcion validafechaentrada : ERROR - El numero del mes debe ser mayor o igual a 1"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	if [ $mes -gt 12 ]
	then 
		printlog 1 "Funcion validafechaentrada : ERROR - El numero del mes debe ser menor o igual a 12"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	
	#Validacion del dia
	dia=`expr substr $1 7 2`
	if [ $dia -lt 1 ]
	then
		printlog 1 "Funcion validafechaentrada : ERROR - El num. del dia debe ser mayor o igual a 1"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	if [ $dia -gt 31 ]
	then 
		printlog 1 "Funcion validafechaentrada : ERROR - El numero del dia debe ser menor o igual a 31"
		SALIR 2  " FALLIDO - Formato Fecha Proceso debe ser YYYYMMDD"
	fi
	return 1
}




	
	#---------------------------------------------------------------------
	#  Validacion cantidad de parametro
	#---------------------------------------------------------------------
	if [ $# -eq 0 -o $# -gt 2 ]; then 
	
	printlog 1 " ERROR DE PARAMETRO(S)"; 
	printlog 1 "PARA EXTRACCION MASIVA DEL POSTGRESS"
	printlog 1 "Un parametro: Fecha inicial (AAAAMMDD)"
	printlog 1 "Ejemplo: 'sh_cons_clie_ext.sh 19000101 APR_Clientes' "
	printlog 1 ""
	printlog 1 "PARA EXTRAER UN PERIODO DEL POSTGRESS: "
	printlog 1 "Dos parametros: Fecha inicial (AAAAMMDD); fecha final (AAAAMMDD) "
	printlog 1 "Ejemplo: 'sh_Apr_Carga.sh 19000101 20211001' "
	printlog 1 " "
	
	SALIR 2  " FALLIDO - La cantidad de parametros no son los requeridos" 
	fi


	if    [ $# -eq 1 ]  
	then
		fec_ini=$1
		validafechaentrada $fec_ini
		printlog 2 "Se cargaran todos los registros del archivo en la tabla correspondiente para el fec_ini $1"
		printlog 1 "Se cargaran todos los registros del archivo en la tabla correspondiente para el fec_ini $1"
		Indicador_Parametro="M"
		export Indicador_Parametro

	elif  [ $# -eq 2 ]
	then 
		fec_ini=$1
		validafechaentrada $fec_ini
		fec_fin=$2
		validafechaentrada $fec_fin
		printlog 1 "Se cargaran todos los registros del archivo en la tabla correspondiente para el fec_ini $1"
		printlog 2 "Se cargaran todos los registros del archivo en la tabla correspondiente para el fec_ini $1"
		printlog 1 "Se cargaran todos los registros del archivo en la tabla correspondiente para el fec_fin $2"
		printlog 2 "Se cargaran todos los registros del archivo en la tabla correspondiente para el fec_fin $2"
		Indicador_Parametro="P"
		export Indicador_Parametro
	fi	
	
echo " el parametro es :" $Indicador_Parametro



valida_definicion_variables () {
if [ "x$1" = "x" ]
then
	printlog 1 "Funcion valida_definicion_variables - Error, no hay variables a validar"
	SALIR 2  " FALLIDO - No se han definido variables"
fi

for V in $1
do
	if [ "x$V" = "x" ]
	then
		printlog 1 "Funcion valida_definicion_variables - ERROR: La variable de ambiente $V no esta definida"
		SALIR 2 " FALLIDO - No se han definido la variable $V"
	else
		printlog 2 "Funcion valida_definicion_variables - La variable de ambiente $V se encuentra definida"
	fi
done
}

# Elimina el archivo validando previamente si existe
eliminar_archivo () 
{
	if [ -f $1 ]
	then
		rm -f $1
		printlog 2 "Se elimina archivo $1"
	else
		printlog 2 "Archivo $1 no puede ser eliminado. Motivo:"
		if [ -d $1 ]
		then
			printlog 2 "Archivo $1 es un directorio."
		else
			printlog 2 "Archivo $1 no existe"
		fi
	fi
}

# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t sh ${INF_PGM} FECHA_PROCESO"
	printlog 1 ""
	printlog 1 "Ejemplo:"
	printlog 1 "sh ${INF_PGM} 20151004"

}



#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de consults sql
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/Cons_Cli_Profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile



#valida definicion de variables de ambiente
valida_definicion_variables "${VARIABLE_AMBIENTE}"



##############################################################
################### INICIO EJECUCION SHELL ###################
##############################################################

# Si no se ha asignado un archivo LOG como parametro, se define uno por defecto.
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${AHORA}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

printlog 2 "Valida si debe sobreescribir el log"
if [ "x$INF_SOBRELOG"="xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi



##Funcion uypuser conexion a bd postgres
setuservar() 
{
   UYPRSP=`UyPuser $1`
   if [[ $? != 0 ]] then
      printlog 2 "ERROR : Se cancelo la ejecucion de UyPuser"
      printlog 2 "ERROR : SISTEMA = $1 "
      printlog 2 "ERROR : RETORNO = ${UYPRSP}"
      P_STAT="ERR"
   else
      uypcnt=0
      for uyprsp in ${UYPRSP}
      do
        uypDa[$uypcnt]=$uyprsp
        (( uypcnt=$uypcnt+1))
      done
            if [ "${uypDa[0]}" = "ERROR" ] ; then
               printlog 2 "ERROR : No se pudo obtener la clave del sistema $1"
               printlog 2 "ERROR : RETORNO = ${uypDa[1]}"
               printlog 2 "ERROR : se aborta ejecucion"
            else
               P_USUARIO=${uypDa[0]}
               P_CLAVE=${uypDa[1]}
               P_STAT="OK"
            fi
   fi
   printlog 2 "- Obtencion de clave para sistema ( $1 ) ${P_STAT}"
}




#---------------------------------------------------------------------
#  Definicion de valiables locales
#---------------------------------------------------------------------

P_USUARIO=""
P_CLAVE=""
P_STAT=OK

printlog 2 "*********************************************************"
printlog 2 "** EJECUCION: ${INF_PGM}  PID=${INF_PID}   ${INF_INI} "
printlog 2 "*********************************************************"



#----------------------------------
# Obtencion de usuario y password
#----------------------------------
setuservar "${sist}"

if [[ "${P_STAT}" = "ERR" ]] ; then
   SALIR 2 "Obtencion de usuario y password incorrecto"
fi

case $Indicador_Parametro in

   M)
   
	# Verifica archivo de consulta sql
		func_valida_archivo $consulta
		
		fec_ini01=$(expr substr $fec_ini 1 4)'-'$(expr substr $fec_ini 5 2)'-'$(expr substr $fec_ini 7 2)
		fec_ini1="'$fec_ini01'"
		
		echo " fecha ini :" $fec_ini1
		
		#nombre de archivo carga masiva de salida con fecha inicial, para cargar en postgres
		archivo_fec=${Nom_ach_entrada}_${fec_ini}.txt
		echo "nombre archivo    " $archivo_fec
		
		# Funcion conexion a bd postgres

		PGPASSWORD=${P_CLAVE} psql -h $server -p $puerto -U $P_USUARIO$useramb -d $bd -v v1=$fec_ini1 -f $consulta > $DirFtpIn/${archivo_fec}
		
		
		#--------------------------------------------------------------------------------
		#Siempre validar que el comando se ejecuto en forma correcta
		#--------------------------------------------------------------------------------
		if [[ $? != 0 ]] then
			printlog 2 "ERROR : La ejecucion de consulta"
			P_STAT="ERR"
		else
			P_STAT="OK"
		fi
		printlog 2 "- Ejecuccion de consulta y generacion de archivo de salida $P_STAT"
		
	
		
		;; #fin condicion M
		
		
	P)
		# Verifica archivo de consulta_02 sql
		func_valida_archivo $consulta02
		
		fec_ini01=$(expr substr $fec_ini 1 4)'-'$(expr substr $fec_ini 5 2)'-'$(expr substr $fec_ini 7 2)
		fec_ini1="'$fec_ini01'"

		fec_fin02=$(expr substr $fec_fin 1 4)'-'$(expr substr $fec_fin 5 2)'-'$(expr substr $fec_fin 7 2)
		fec_fin2="'$fec_fin02'"
		
		echo " fecha ini :" $fec_ini1
		echo " fecha fin :" $fec_fin2

		#nombre de archivo carga ñperiodo de salida con fecha inicial y fecha final, para cargar en postgres
		archivo_fec=${Nom_ach_entrada}_${fec_ini}_${fec_fin}.txt
		echo "nombre archivo    " $archivo_fec
		
		# Funcion conexion a bd postgres
	
		PGPASSWORD=${P_CLAVE} psql -h $server -p $puerto -U $P_USUARIO$useramb -d $bd -v v1=$fec_ini1 -v v2=$fec_fin2 -f $consulta02 > $DirFtpIn/${archivo_fec}
				
		#--------------------------------------------------------------------------------
		#Siempre validar que el comando se ejecuto en forma correcta
		#--------------------------------------------------------------------------------
		if [[ $? != 0 ]] then
			printlog 2 "ERROR : La ejecucion de consulta02"
			P_STAT="ERR"
		else
			P_STAT="OK"
		fi
		printlog 2 "- Ejecuccion de consulta02 y generacion de archivo de salida $P_STAT"
		
		
		
		;; #fin condicion P
		

esac


# -------------------- FUNCIONES EXCLUSIVAS SQL SERVER --------------------------

Sb_Realiza_Conexion () 
{
	printlog 1 "Funcion Sb_Realiza_Conexion - El sistema al obtener es $1"
	P_USUARIO=""
	printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_USUARIO: $P_USUARIO"
	P_CLAVE=""
	printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_CLAVE: $P_CLAVE"
	UYPRSP=$(UyPuser $1)
	if [[ $? != 0 ]]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Se cancelo la ejecucion de UyPuser"
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : SISTEMA = $1 "
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : RETORNO = ${UYPRSP}"
		P_STAT="ERR"
		printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_STAT: $P_STAT"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	else
		uypcnt=0
		for uyprsp in ${UYPRSP}
		do
			printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: uypcnt: $uypcnt"
			uypDa[$uypcnt]=$uyprsp
			(( uypcnt=$uypcnt+1))
		done
		if [ "${uypDa[0]}" = "ERROR" ] ; then
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : No se pudo obtener la clave del sistema $1"
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : RETORNO = ${uypDa[1]}"
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : se aborta ejecucion"
			SALIR 2 " FALLIDO - Error al obtener UyPuser."
		else
			P_USUARIO=${uypDa[0]}
			P_CLAVE=${uypDa[1]}
			printlog 1 "Funcion Sb_Realiza_Conexion - Se obtiene la informacion del sistema $1"
			printlog 2 "Funcion Sb_Realiza_Conexion - Usuario obtenido : ${P_USUARIO}"
			printlog 2 "Funcion Sb_Realiza_Conexion - Clave no registrada, pero obtenida"
		fi
fi
	printlog 2 "Funcion Sb_Realiza_Conexion - Validacion de nombre de usuario"
	if [ x"$P_USUARIO" = "x" ]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Usuario obtenido es vacio [Sistema = $1]"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	fi
	if [ x"$P_CLAVE" = "x" ]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Clave obtenida es vacia [Sistema = $1]"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	fi
	
	export P_USUARIO
	export P_CLAVE

	
	printlog 1 "Funcion Sb_Realiza_Conexion - Obtenidos los datos correctamente."
}


Sb_Elimina_Registros()
{
	printlog 1 "Funcion Sb_Elimina_Registros - Se ingresa a la funcion"
	printlog 2 "Funcion Sb_Elimina_Registros - Parmetro 1 - Base de Datos: $1"
	printlog 2 "Funcion Sb_Elimina_Registros - Parmetro 2 - Tabla: $2"
	printlog 2 "Funcion Sb_Elimina_Registros - Parmetro 3 - Fecha de carga a borrar: \"$3\""

#ELIMINA EN CASO DE REPROCESO


fisql -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} << eof >> $DirTmp/${INF_PID}_SqlServer.tmp
use $1 
go
delete from dbo.$2
go
eof
	RETORNO=$?
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO != 0 ]] then 
		printlog 1 "Funcion Sb_Elimina_Registros - ERROR : El borrado de registros de la tabla $2 se cancelo"
		SALIR 2 "FALLIDO - El borrado de registros de la tabla $2 se cancelo"
	elif [ -f $DirTmp/${INF_PID}_SqlServer.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirTmp/${INF_PID}_SqlServer.tmp`
		if [[ "${verE}" != "0" ]]; then
			printlog 2 "Funcion Sb_Elimina_Registros - ERROR : El borrado de registros de la tabla $2 retorno una excepcion"
			SALIR 2 "FALLIDO - El borrado de registros de la tabla $2 retorno una excepcion"
		fi
	fi
	
	printlog 1 "Funcion Sb_Elimina_Registros - Los registros se han eliminado exitosamente"
	printlog 2 "Funcion Sb_Elimina_Registros - Se respalda Log de la eliminacion"
	cat $DirTmp/${INF_PID}_SqlServer.tmp >> ${archLog}
	eliminar_archivo $DirTmp/${INF_PID}_SqlServer.tmp
	
}


Sb_Realiza_BcpIn ()
{ # Paratmeros: $1 = BD	 $2 = nombre_tabla	$3 = Ruta/nombre_archivo $4 = separador $5 = RETORNO (si en vez de salir con error debe escalar el codigo)

printlog 1 "Funcion Sb_Realiza_BcpIn - Se ingresa a la funcion"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 1 - Base de Datos: $1"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 2 - Tabla: $2"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 3 - nombre_archivo: $3"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 4 - Separador: $4"


printlog 2 "Funcion Sb_Realiza_BcpIn - Variable externa - Motor: ${SbMotor}"
printlog 2 "Funcion Sb_Realiza_BcpIn - Variable externa - Usuario: ${P_USUARIO}"

cd $DirFtpIn
if [ -s $3 ]
	 then
	numreg=$(wc -l $3| awk '{ print $1 }')
	printlog 1 "Funcion Sb_Realiza_BcpIn - Registros a Cargar en tabla $2: $numreg"  
	
	
	echo 
	
	freebcp $1..$2 in $3 -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} -c -b1000 -t "$4" -e ${INF_PID}_SqlServer_bcp_err.tmp >> $DirFtpIn/${INF_PID}_SqlServer.tmp > temp_bcp.txt
	RETORNO=$?
	
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO -ne 0 ]]
	then 
		printlog 1 "Funcion Sb_Realiza_BcpIn - ERROR : El BCP IN a la tabla $2 se cancelo"
		SALIR 2 "Funcion Sb_Realiza_BcpIn - ERROR : El BCP IN a la tabla $2 se cancelo"
	elif [ -f $DirFtpIn/${INF_PID}_SqlServer.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirFtpIn/${INF_PID}_SqlServer.tmp`

		if [[ "${verE}" != "0" ]]; then
			printlog 1 "Funcion Sb_Realiza_BcpIn - ERROR : BCP IN a la tabla $2 retorno una excepcion"
			SALIR 2 "Funcion Sb_Realiza_BcpIn - ERROR : BCP IN a la tabla $2 retorno una excepcion"
		fi
	fi
	
	mv $archivo_fec $DirFtpOut/$archivo_fec
	
	printlog 1 "Funcion Sb_Realiza_BcpIn - El archivo $3 se ha cargado correctamente"
	printlog 2 "Funcion Sb_Realiza_BcpIn - Se respalda Log de carga"
	cat $DirFtpIn/${INF_PID}_SqlServer.tmp >> ${archLog}
	eliminar_archivo $DirFtpIn/${INF_PID}_SqlServer.tmp
	eliminar_archivo temp_bcp.txt
	
	
fi
}


Sb_Realiza_Conexion $SbUyPSist

Sb_Elimina_Registros $SbBD $tbl $fechasql

Sb_Realiza_BcpIn $SbBD $tbl $archivo_fec $separador


printlog 1 "Ejecucion Correcta"
SALIR 0  "CORRECTO"


