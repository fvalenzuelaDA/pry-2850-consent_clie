#!/usr/bin/sh
#***************************************************************************
# Nombre      : sh_BciPass_cons_clie_ext.sh
# Ruta        : /MIGCON/she/
# Autor       : Francisco Valenzuela (Ada Ltda.) - Claudio Bello (Ing. Software. Bci)
# Fecha       : 08/11/2021
# Descripcion : Carga informacion de consentimiento de cliente desde BciPass
# Parametros  : no aplica
# Ejemplo de Ejecucion  : sh sh_BciPass_cons_clie_ext.sh
#***************************************************************************
# Mantencion  :
# Empresa     :
# Fecha       :
# Descripcion :
#***************************************************************************


#--------------------------------------------------------------------------------
#	Carga Informacion de Ejecucion
#--------------------------------------------------------------------------------
	INF_PGM=`echo $0 |sed s/"\/"/" "/g|awk '{print $(NF)}'`
	BSE_PGM=`echo ${INF_PGM} |cut -d"." -f1`
	INF_PID=$$
	INF_ARG=$@
	INF_CANT_ARG=$#
	INF_STT=0
	INF_INI=`date +%d/%m/%y" "%H:%M:%S`
	AHORA=`date +'%Y%m%d%H%M'`

# LOG PREVIO A LA CARGA DEL ARCHIVO PROFILE
archLog=$HOME/tmp/${BSE_PGM}_${INF_PID}.log
export archLog


#--------------------------------------------------------------------------------
#	DEFINICION DE FUNCIONES BASICAS
#--------------------------------------------------------------------------------
unifica_archivos_log()
{
	printlog 1 "Unificando en el log archivos para su analisis"

	if [ -f ${DirTmp}/${INF_PID}*.tmp ]
	then
		for FILE in ${DirTmp}/${INF_PID}*.tmp
		do
			printlog 2 "-------------------------------------------------------------"
			printlog 2 "Archivo: $FILE"
			printlog 2 "Contenido:"
			printlog 2 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			cat $FILE| grep -i -v "\.logon" >> ${archLog}
			printlog 2 "--------------------- FIN DE ARCHIVO ------------------------"
			eliminar_archivo $FILE
		done
	fi
}

printlogfinal()
{	
	printlog 2 "valida si log existe"
	if [ "x${archLogFnl}" = "x" ]
	then
		#Si el nombre del log real no se ha construido debido a que no se obtuvo el parametro de
		#fecha para la extension
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			 "correctos."
		echo "Favor de ver log de paso ${archLog}"
		echo "No se ha podido generar el archivo log del proceso, dado que los parametros son in"\
			"correctos." >> ${archLog}
		echo "Favor de ver log de paso ${archLog}" >> ${archLog}
		return
	fi
	
	printlog 2 "valida si log tiene datos"
	if [ -s ${archLogFnl} ]
	then
		# Si existe el log final, quiere decir que ya existe una ejecucion del proceso para
		# la misma fecha, por lo cual, se debe concatenar el resultado de esta ejecucion
		# con el resultado de la ejecucion anterior en el mismo archivo.
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" 
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "######### --->                  INICIO EJECUCION `date +%d/%m/%y`                  "\
			"<--- #############" >> ${archLogFnl}
		echo "###################################################################################"\
			"##################" >> ${archLogFnl}
		echo "##### ##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
		echo "" ${archLogFnl}
		cat ${archLog} >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "" >> ${archLogFnl}
		echo "##### Fin de la ejecucion  `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"\
			>> ${archLogFnl}
		rm -f ${archLog}
	else
		echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####"
		echo "" > ${archLogFnl}
		
		# Si el archivo de log final no existe pero se puedo crear con la linea anterior, se tras-
		#pasa el resultado de la ejecucion del log temporal al log final.
		echo "Valida si log final fue creado correctamente" >> ${archLogFnl}
		if test -s ${archLogFnl}
		then
			echo "" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "####### --->                  INICIO EJECUCION `date +%d/%m/%y`                "\
					"<--- ###############" >> ${archLogFnl}
			echo "###############################################################################"\
					"######################" >> ${archLogFnl}
			echo "##### Se realiza traspaso del log temporal al log final con fecha `date +%d/%m/%y\" \"%H:%M:%S` #####" >> ${archLogFnl}
			echo "" ${archLogFnl}
			cat ${archLog} >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "" >> ${archLogFnl}
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###"
			echo "##### Fin de la ejecucion del proceso con fecha `date +%d/%m/%y\" \"%H:%M:%S` ##"\
				"###" >> ${archLogFnl}
			rm -f ${archLog}
		fi
	fi
}

SALIR()
{
	# $1 Codigo de salida
	# $2 o mas, mensaje de salida
	printlog 2 "valida retorno de salida"
	if [ $1 -gt 0 ]
	then
		printlog 2 "La funcion salir recibe un error como codigo de salida. Codigo recibido $1"
		unifica_archivos_log
	fi
	printlog 1 "************************************************************************************"
	printlog 1 "**                     RESUMEN                                                    **"
	printlog 1 "************************************************************************************"
	printlog 1 "Servidor      : `uname -n`"
	printlog 1 "Usuario       : `whoami`" 
	printlog 1 "Programa      : ${INF_PGM}"
	printlog 1 "ID proceso    : ${INF_PID}"
	printlog 1 "Hora_Inicio   : ${INF_INI}"
	printlog 1 "Hora Termino  : `date +%d/%m/%y\" \"%H:%M:%S`"
	printlog 1 "Parametros    : ${INF_ARG}"
	printlog 1 "Cant. Parametr: ${INF_CANT_ARG}"
	printlog 1 "Resultado     : ${2} "
	printlog 1 "Archivo Log   : ${archLogFnl}"
	printlog 1 "************************************************************************************"
	echo $2 | tee -a ${archLog}
	printlogfinal
	exit $1
}

printlog()
{
	# Parametro 1: Usar "1" para que la informacion salga por pantalla y quede en el log o "2" para que solo quede en el log
	# Parametro 2: El contenido a desplegar
	Hora=$(date +%H:%M:%S)
	echo "${Hora} valida printlog"   >> ${archLog}
	if [ $1 -eq 1 -o "x${INF_PRINTLOG}" = "xSI" ]
	then
		echo ${Hora} "$2" # Salida a Consola
	fi
	echo ${Hora} "$2"  >> ${archLog}
}
#----------------------------------------------------
#	Funciones previa carga del archivo de funciones.
#----------------------------------------------------

# Funcion que valida la existencia de un archivo y que tenga mas de 0 byte
func_valida_archivo ()
{
	printlog 2 "valida la existencia de un archivo"
	if [ ! -f $1 ]
	then 
		printlog 1 "Funcion func_valida_archivo : ERROR No se encuentra el archivo $1"
		SALIR 2 "FALLIDO - No se encuentra el archivo $1"
	fi

	#verifica archivo distinto de vacio
	printlog 2 "verifica archivo distinto de vacio"
	if [ -s $1 ]
	then
		printlog 2 "Funcion func_valida_archivo : Archivo $1 OK"
	else
		printlog 2 "Funcion func_valida_archivo : ERROR - El archivo $1 no posee datos."
		SALIR 2 "FALLIDO - El archivo $1 no posee datos"
	fi
}

# Funcion que valida la ejecucion o carga de un archivo (Profile y funciones)
func_valida_carga_archivo ()
{
	printlog 1 "Funcion func_valida_carga_archivo : Validando carga del $2 de Profile..."
	if [ $1 -ne 0 ]
	then
		printlog 2 "Funcion func_valida_carga_archivo : ERROR Archivo $2 no cargado."
		SALIR 2 "FALLIDO - Archivo $2 no cargado."
	else
		printlog 1 "Funcion func_valida_carga_archivo : Archivo $2 encontrado y cargado."
	fi
}

	
Valida_num_param()
{
	# $1 Cantidad de parametros a validar
	# $2 Cantidad de parametros correctas
	
	printlog 2 "Funcion Valida_num_param - Parametro 1: $1"
	printlog 2 "Funcion Valida_num_param - Parametro 2: $2"
	
	if [ $1 -ne $2 ] 
	then
		printlog 2 "Funcion Valida_num_param : ERROR - No se requiere de parametros"
		Syntax
		SALIR 2  " FALLIDO - No se requiere de parametros"
	fi
}


valida_definicion_variables () {
if [ "x$1" = "x" ]
then
	printlog 1 "Funcion valida_definicion_variables - Error, no hay variables a validar"
	SALIR 2  " FALLIDO - No se han definido variables"
fi

for V in $1
do
	if [ "x$V" = "x" ]
	then
		printlog 1 "Funcion valida_definicion_variables - ERROR: La variable de ambiente $V no esta definida"
		SALIR 2 " FALLIDO - No se han definido la variable $V"
	else
		printlog 2 "Funcion valida_definicion_variables - La variable de ambiente $V se encuentra definida"
	fi
done
}

# Elimina el archivo validando previamente si existe
eliminar_archivo () 
{
	if [ -f $1 ]
	then
		rm -f $1
		printlog 2 "Se elimina archivo $1"
	else
		printlog 2 "Archivo $1 no puede ser eliminado. Motivo:"
		if [ -d $1 ]
		then
			printlog 2 "Archivo $1 es un directorio."
		else
			printlog 2 "Archivo $1 no existe"
		fi
	fi
}

# Funcion que describe como invocar el proceso
Syntax ()
{
	printlog 1 "Sintaxis:"
	printlog 1 "\t sh ${INF_PGM}"
	printlog 1 ""
	printlog 1 "Ejemplo:"
	printlog 1 "sh ${INF_PGM}"

}



#--------------------------------------------------------------------------------
# Definicion de archivo Profile y de consults sql
#--------------------------------------------------------------------------------
ArchivoProfile=$HOME/cfg/Cons_Cli_Profile

# Verifica archivo Profile
func_valida_archivo $ArchivoProfile

. $ArchivoProfile
func_valida_carga_archivo $? $ArchivoProfile


#valida definicion de variables de ambiente
valida_definicion_variables "${VARIABLE_AMBIENTE}"


#valida parametro de entrada
Valida_num_param $# $Cant_Min_Param


##############################################################
################### INICIO EJECUCION SHELL ###################
##############################################################

# Si no se ha asignado un archivo LOG como parametro, se define uno por defecto.
printlog 2 "Asignacion del log final"
if [ "x${archLogFnl}" = "x" ]
then
	archLogFnl=${DirLog}/${BSE_PGM}.${AHORA}.log
	printlog 2 "Asignacion de Variable: archLogFnl=${archLogFnl}"
fi

printlog 2 "Valida si debe sobreescribir el log"
if [ "x$INF_SOBRELOG"="xSI" ]
then
	eliminar_archivo ${archLogFnl}
fi



# -------------------- FUNCIONES EXCLUSIVAS SQL SERVER --------------------------

Sb_Realiza_Conexion () 
{
	printlog 1 "Funcion Sb_Realiza_Conexion - El sistema al obtener es $1"
	P_USUARIO=""
	printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_USUARIO: $P_USUARIO"
	P_CLAVE=""
	printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_CLAVE: $P_CLAVE"
	UYPRSP=$(UyPuser $1)
	if [[ $? != 0 ]]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Se cancelo la ejecucion de UyPuser"
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : SISTEMA = $1 "
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : RETORNO = ${UYPRSP}"
		P_STAT="ERR"
		printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: P_STAT: $P_STAT"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	else
		uypcnt=0
		for uyprsp in ${UYPRSP}
		do
			printlog 2 "Funcion Sb_Realiza_Conexion - Asignacion de Variable: uypcnt: $uypcnt"
			uypDa[$uypcnt]=$uyprsp
			(( uypcnt=$uypcnt+1))
		done
		if [ "${uypDa[0]}" = "ERROR" ] ; then
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : No se pudo obtener la clave del sistema $1"
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : RETORNO = ${uypDa[1]}"
			printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : se aborta ejecucion"
			SALIR 2 " FALLIDO - Error al obtener UyPuser."
		else
			P_USUARIO=${uypDa[0]}
			P_CLAVE=${uypDa[1]}
			printlog 1 "Funcion Sb_Realiza_Conexion - Se obtiene la informacion del sistema $1"
			printlog 2 "Funcion Sb_Realiza_Conexion - Usuario obtenido : ${P_USUARIO}"
			printlog 2 "Funcion Sb_Realiza_Conexion - Clave no registrada, pero obtenida"
		fi
fi
	printlog 2 "Funcion Sb_Realiza_Conexion - Validacion de nombre de usuario"
	if [ x"$P_USUARIO" = "x" ]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Usuario obtenido es vacio [Sistema = $1]"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	fi
	if [ x"$P_CLAVE" = "x" ]; then
		printlog 1 "Funcion Sb_Realiza_Conexion - ERROR : Clave obtenida es vacia [Sistema = $1]"
		SALIR 2 " FALLIDO - Error al obtener UyPuser."
	fi
	
	export P_USUARIO
	export P_CLAVE

	
	printlog 1 "Funcion Sb_Realiza_Conexion - Obtenidos los datos correctamente."
}


Sb_Elimina_Registros()
{
	printlog 1 "Funcion Sb_Elimina_Registros - Se ingresa a la funcion"
	printlog 2 "Funcion Sb_Elimina_Registros - Parmetro 1 - Base de Datos: $1"
	printlog 2 "Funcion Sb_Elimina_Registros - Parmetro 2 - Tabla: $2"

#ELIMINA EN CASO DE REPROCESO


fisql -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} << eof >> $DirTmp/${INF_PID}_SqlServer.tmp
use $1 
go
delete from dbo.$2
go
eof
	RETORNO=$?
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO != 0 ]] then 
		printlog 1 "Funcion Sb_Elimina_Registros - ERROR : El borrado de registros de la tabla $2 se cancelo"
		SALIR 2 "FALLIDO - El borrado de registros de la tabla $2 se cancelo"
	elif [ -f $DirTmp/${INF_PID}_SqlServer.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirTmp/${INF_PID}_SqlServer.tmp`
		if [[ "${verE}" != "0" ]]; then
			printlog 2 "Funcion Sb_Elimina_Registros - ERROR : El borrado de registros de la tabla $2 retorno una excepcion"
			SALIR 2 "FALLIDO - El borrado de registros de la tabla $2 retorno una excepcion"
		fi
	fi
	
	printlog 1 "Funcion Sb_Elimina_Registros - Los registros se han eliminado exitosamente"
	printlog 2 "Funcion Sb_Elimina_Registros - Se respalda Log de la eliminacion"
	cat $DirTmp/${INF_PID}_SqlServer.tmp >> ${archLog}
	eliminar_archivo $DirTmp/${INF_PID}_SqlServer.tmp
	
}



Sb_Extrae_Registros()
{
	printlog 1 "Funcion Sb_Extrae_Registros - Se ingresa a la funcion"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 1 - Base de Datos: $1"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 2 - Campo: $2"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 3 - Tabla: $3"
	printlog 2 "Funcion Sb_Extrae_Registros - Parmetro 4 - Condicion: \"$4\""
	
#ELIMINA EN CASO DE REPROCESO


fisql -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} << eof >> $DirTmp/${INF_PID}_SqlServer_dato.tmp
use $1 
go
select $2 from dbo.$3 where ori_nombre = '$4'
go
eof
	RETORNO=$?
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO != 0 ]] then 
		printlog 1 "Funcion Sb_Extrae_Registros - ERROR : La extraccion de registros de la tabla $3 se cancelo"
		SALIR 2 "FALLIDO - La extraccion de registros de la tabla $3 se cancelo"
	elif [ -f $DirTmp/${INF_PID}_SqlServer_dato.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirTmp/${INF_PID}_SqlServer_dato.tmp`
		if [[ "${verE}" != "0" ]]; then
			printlog 2 "Funcion Sb_Extrae_Registros - ERROR : La extraccion de registros de la tabla $3 retorno una excepcion"
			SALIR 2 "FALLIDO - La extraccion de registros de la tabla $3 retorno una excepcion"
		fi
	fi
	
	sin_valor=`sed -n 1p $DirTmp/${INF_PID}_SqlServer_dato.tmp | awk 'BEGIN {FS=";"}; {print $1}'`
	export sin_valor
	
	if [[ $sin_valor = '(0 rows affected)' ]] then 
		printlog 1 "Funcion Sb_Extrae_Registros - ERROR : No existen origen id en la tabla $3, para ENROLAMIENTO BCIPASS "
		SALIR 2 "SE CANCELA - No existe origen id en la tabla $3, para ENROLAMIENTO BCIPASS"
	fi
	
	printlog 1 "Funcion Sb_Extrae_Registros - Los registros se han extraido exitosamente"
	printlog 2 "Funcion Sb_Extrae_Registros - Se respalda Log de la extraccion"
	
	#Elimina espacios y tabulaciones al final de una linea
	sed 's/[ \t]*$//' $DirTmp/${INF_PID}_SqlServer_dato.tmp >> $DirTmp/${INF_PID}_SqlServer_dato_sin_espacio.tmp

	#registro obtenido de 
	valor=`sed -n 3p $DirTmp/${INF_PID}_SqlServer_dato_sin_espacio.tmp | awk 'BEGIN {FS=";"}; {print $1}'`
	export valor

	cat $DirTmp/${INF_PID}_SqlServer_dato.tmp >> ${archLog}
	eliminar_archivo $DirTmp/${INF_PID}_SqlServer_dato.tmp
	eliminar_archivo $DirTmp/${INF_PID}_SqlServer_dato_sin_espacio.tmp
	
}



Sb_Realiza_BcpIn ()
{ # Paratmeros: $1 = BD	 $2 = nombre_tabla	$3 = Ruta/nombre_archivo $4 = separador $5 = RETORNO (si en vez de salir con error debe escalar el codigo)

printlog 1 "Funcion Sb_Realiza_BcpIn - Se ingresa a la funcion"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 1 - Base de Datos: $1"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 2 - Tabla: $2"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 3 - nombre_archivo: $3"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 4 - Separador: $4"
printlog 2 "Funcion Sb_Realiza_BcpIn - Parametro 5 - Nombre dela archivo: $4"


printlog 2 "Funcion Sb_Realiza_BcpIn - Variable externa - Motor: ${SbMotor}"
printlog 2 "Funcion Sb_Realiza_BcpIn - Variable externa - Usuario: ${P_USUARIO}"

cd $DirFtpIn
if [ -s $3 ]
	 then
	numreg=$(wc -l $3| awk '{ print $1 }')
	printlog 1 "Funcion Sb_Realiza_BcpIn - Registros a Cargar en tabla $2: $numreg"  

	
	freebcp $1..$2 in $3 -U${P_USUARIO} -P${P_CLAVE} -S${SbMotor} -c -b1000 -t "$4" -e ${INF_PID}_SqlServer_bcp_err.tmp >> $DirFtpIn/${INF_PID}_SqlServer.tmp > temp_bcp.txt
	RETORNO=$?
	
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $RETORNO -ne 0 ]]
	then 
		printlog 1 "Funcion Sb_Realiza_BcpIn - ERROR : El BCP IN a la tabla $2 se cancelo"
		SALIR 2 "Funcion Sb_Realiza_BcpIn - ERROR : El BCP IN a la tabla $2 se cancelo"
	elif [ -f $DirFtpIn/${INF_PID}_SqlServer.tmp ]; then
		verE=`egrep -c 'Msg |CT-LIBRARY error:|CS-LIBRARY error:|CSLIB Message:' $DirFtpIn/${INF_PID}_SqlServer.tmp`

		if [[ "${verE}" != "0" ]]; then
			printlog 1 "Funcion Sb_Realiza_BcpIn - ERROR : BCP IN a la tabla $2 retorno una excepcion"
			SALIR 2 "Funcion Sb_Realiza_BcpIn - ERROR : BCP IN a la tabla $2 retorno una excepcion"
		fi
	fi
	
	mv $DirFtpIn/$Nom_ach_ent_BciPass_02 $DirFtpOut/$Nom_ach_ent_BciPass_02
	
	printlog 1 "Funcion Sb_Realiza_BcpIn - El archivo $5 se ha cargado correctamente"
	printlog 2 "Funcion Sb_Realiza_BcpIn - El archivo $5 se ha cargado correctamente, Se respalda Log de carga"
	cat $DirFtpIn/${INF_PID}_SqlServer.tmp >> ${archLog}
	eliminar_archivo $DirFtpIn/${INF_PID}_SqlServer.tmp
	eliminar_archivo temp_bcp.txt
	
	
fi
}


Sb_Valida_Cant_arch ()
{
	# $1 Cantidad de archivos detectados
	# $2 Cantidad de archivos permitidos
	# $3 Nombre del archivo a cargar
	
	#--------------------------------------------------------------------------------
	# Siempre validar que el comando se ejecuto en forma correcta
	#--------------------------------------------------------------------------------
	if [[ $1 -gt $2 ]] then 
		printlog 1 "Funcion Sb_Valida_Cant_arch - ERROR : Existe $cantidad archivos $3%.txt con el mismo nombre, se cancelo"
		SALIR 2 "FALLIDO - Existe $cantidad archivos $3%.txt con el mismo nombre, se cancelo"
		
			printlog 2 "Funcion Sb_Valida_Cant_arch - ERROR : Existe $cantidad archivos $3%.txt con el mismo nombre, se cancelo"
			SALIR 2 "FALLIDO - Existe $cantidad archivos $3%.txt con el mismo nombre, se cancelo"
	fi
}	



Sb_Valida_Cant_Columnas ()
{

cat ${1} | awk ' length($0)> 0' | awk -F';' 'NF!=26' > ${DirTmp}/val_colum.txt

if [ -s ${DirTmp}/val_colum.txt ]; then
		printlog 1 "Funcion Sb_Valida_Cant_Columnas - ERROR : El archivo ${1} tiene lineas con formato incorrecto de columnas, se cancelo"
		printlog 2 "Funcion Sb_Valida_Cant_Columnas - ERROR : El archivo ${1} tiene lineas con formato incorrecto de columnas, se cancelo"
		
		eliminar_archivo ${DirTmp}/val_colum.txt
		
		SALIR 2 "FALLIDO - El archivo ${1} tiene lineas con formato incorrecto de columnas, se cancelo"
		
		
fi

}

#valida archivo de entrada, de origen BciPass
func_valida_archivo $DirFtpIn/$Nom_ach_BciPass*
Sb_Valida_Cant_Columnas $DirFtpIn/$Nom_ach_BciPass*

#muestra todos los archivos que empiecen con Respaldo_JEN
find $DirFtpIn/$Nom_ach_BciPass* > ${DirTmp}/lista_jen.txt

#cuenta la cantidad de archivos que empiecen con Respaldo_JEN
grep -c $Nom_ach_BciPass ${DirTmp}/lista_jen.txt > ${DirTmp}/cant_Respaldo_JEN.txt

#garda en variable la cantidad del mismo archivo
cantidad=`awk 'BEGIN {FS=";"}; {print substr($1,1,5)}' ${DirTmp}/cant_Respaldo_JEN.txt `
eliminar_archivo ${DirTmp}/cant_Respaldo_JEN.txt

#valida cantidad de archivos de entrada
Sb_Valida_Cant_arch $cantidad $Cant_Arch_Jen $DirFtpIn/$Nom_ach_BciPass


#extrae nombre de archivo a cargar con directorio
Nom_ach_ent_BciPass=`awk 'BEGIN {FS=";"}; {print substr($1,1,100)}' ${DirTmp}/lista_jen.txt `

#extrae nombre de archivo a cargar sin directorio
Nom_ach_ent_BciPass_02=`awk 'BEGIN {FS=";"}; {print substr($1,12,25)}' ${DirTmp}/lista_jen.txt `
eliminar_archivo ${DirTmp}/lista_jen.txt


#valida archivo de entrada, de origen BciPass
func_valida_archivo $Nom_ach_ent_BciPass


#variable archivo temporal sin cabera
tmp_ach_ent_BciPass=${DirTmp}/tmp_ach_ent_BciPass.txt

#quita cabecera
sed '1,1d' $Nom_ach_ent_BciPass >  $tmp_ach_ent_BciPass

#1.-substrae el dato fecha que viene dentro del archivo
awk 'BEGIN {FS=";"}; {print substr($3,1,10)}' $tmp_ach_ent_BciPass > ${DirTmp}/bcipass_fec.txt

#2.-substrae el dato hora:min:seg que viene dentro del archivo
awk 'BEGIN {FS=";"}; {print substr($3,12,14)}' $tmp_ach_ent_BciPass > ${DirTmp}/bcipass_hora.txt

#3.-cambia formato del dato fecha de dd-mm-yyyy a yyyy-mm-dd
awk 'BEGIN {FS="-"}; {printf "%s-%s-%s\n",";"$3,$2,$1}' ${DirTmp}/bcipass_fec.txt > ${DirTmp}/bcipass_fec_ok.txt
eliminar_archivo ${DirTmp}/bcipass_fec.txt

#4.-junta dato fecha nueva con su respectiva hora:min:seg
paste ${DirTmp}/bcipass_fec_ok.txt ${DirTmp}/bcipass_hora.txt > ${DirTmp}/bcipass_fec_hora_ok.txt
eliminar_archivo ${DirTmp}/bcipass_hora.txt
eliminar_archivo ${DirTmp}/bcipass_fec_ok.txt

#5.- inserta en el archivo original, la nueva fecha ya formateada en los puntos anteriores
paste $tmp_ach_ent_BciPass ${DirTmp}/bcipass_fec_hora_ok.txt > ${DirTmp}/tmp_ach_fec_BciPass.txt
eliminar_archivo ${DirTmp}/bcipass_fec_hora_ok.txt

#reemplaza tabulacion por espacio
sed -e 's/[	]/ /g' ${DirTmp}/tmp_ach_fec_BciPass.txt > ${DirTmp}/tmp_ach_fec_BciPass_ok.txt
eliminar_archivo ${DirTmp}/tmp_ach_fec_BciPass.txt
eliminar_archivo $tmp_ach_ent_BciPass


#nombre del archivo de salida jurnal BciPass para cargar en sql server
Nom_ach_sal_BciPass=${DirFtpIn}/Respaldo_JEN_tmp.txt

Sb_Realiza_Conexion $SbUyPSist
Sb_Extrae_Registros $SbBD $campo $tabla "${condicion}"


awk -v EnrBci="$valor" -v Pol_id="$Politica_id_BciPass" -v Cnl_Bci="$Canal_BciPass" -v Cons="$consentimiento" 'BEGIN {FS=";"}; {print $4 ";" $5 ";" EnrBci ";" $27 ";" Pol_id ";" $12 ";" Cnl_Bci ";" Cons}'  ${DirTmp}/tmp_ach_fec_BciPass_ok.txt> $Nom_ach_sal_BciPass
eliminar_archivo ${DirTmp}/tmp_ach_fec_BciPass_ok.txt

#valida archivo de salida, de origen BciPass, sin cabecera, para ser cargado al sql server
func_valida_archivo $Nom_ach_sal_BciPass

Sb_Elimina_Registros $SbBD $tbl

Sb_Realiza_BcpIn $SbBD $tbl $Nom_ach_sal_BciPass $separador $Nom_ach_ent_BciPass
eliminar_archivo $Nom_ach_sal_BciPass

printlog 1 "Ejecucion Correcta"
SALIR 0  "CORRECTO"
