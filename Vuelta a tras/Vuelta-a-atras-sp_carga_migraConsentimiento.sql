use consentimiento
go

IF OBJECT_ID('dbo.sp_carga_migraConsentimiento') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_carga_migraConsentimiento
    IF OBJECT_ID('dbo.sp_carga_migraConsentimiento') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_carga_migraConsentimiento >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_carga_migraConsentimiento >>>'
END
go