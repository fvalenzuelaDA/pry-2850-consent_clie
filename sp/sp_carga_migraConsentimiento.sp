use consentimiento
go

IF OBJECT_ID('dbo.sp_carga_migraConsentimiento') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.sp_carga_migraConsentimiento
    IF OBJECT_ID('dbo.sp_carga_migraConsentimiento') IS NOT NULL
        PRINT '<<< FAILED DROPPING PROCEDURE dbo.sp_carga_migraConsentimiento >>>'
    ELSE
        PRINT '<<< DROPPED PROCEDURE dbo.sp_carga_migraConsentimiento >>>'
END
go


/* *************************************************************************************************************** */
/* Nombre SP                      : sp_carga_migraConsentimiento                                                 */
/* Nombre BD                      : Consentimiento                                                               */
/* Tipo de ejecución              : Control-M                                                                       */ 
/* Fecha creación                 : 22/11/2021                                                                   */
/* Autor                          : Francisco V. (ADA)		 			  		                         */
/* Objetivos                      : migracion de consentimiento                                                   */
/* Canal de ejecución             : Basc                                                                */
/* Parametros entrada             : no aplica                                                    */
/* Retorno						  : no aplica                                                                    */
/* Ejemplo de ejecución           : exec sp_carga_migraConsentimiento 										     */
/* *************************************************************************************************************** */


CREATE PROCEDURE dbo.sp_carga_migraConsentimiento
AS
        
BEGIN

/* ************************************************* */
/*tabla temporal maxima fecha de registro x cliente*/
/* ************************************************* */
create table #max_fec_cons (
cons_rut int
,cons_dv char(01)
,cons_fec_registro datetime
)

	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No crea tabla temporal #max_fec_cons'
		return -1
	END
	
	
/* ************************************************* */
/*inserta maxima fecha de registro x cliente*/
/* ************************************************* */
insert into #max_fec_cons
select
	cons_rut
	,cons_dv
	,max(cons_fec_registro)
FROM 
	consentimiento.dbo.consentimiento_de_paso
group by cons_rut,cons_dv


	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No inserta en tabla temporal #max_fec_cons'
		return -1
	END

/* ************************************************* */
/*tabla temporal registro consentimiento mas actual */
/* ************************************************* */
CREATE TABLE #cons_new (	
	pol_id int NOT NULL
	,cons_rut int NOT NULL
	,cons_dv varchar(1) NULL
	,ori_id int NULL
	,cons_fec_registro datetime NULL
	,cons_ip varchar(15)
	,cons_canal varchar(3) NULL
	,cons_consentimiento varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	CONSTRAINT pk_cons_new PRIMARY KEY (cons_rut)
)

	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No crea tabla temporal #cons_new'
		return -1
	END

/* ************************************************* */
/*inserta registro consentimiento mas actual */
/* ************************************************* */
insert into #cons_new
select
	a.pol_id
	,a.cons_rut
	,a.cons_dv
	,a.ori_id
	,a.cons_fec_registro
	,a.cons_ip
	,a.cons_canal
	,REPLACE(REPLACE(REPLACE(a.cons_consentimiento,'polÃƒÂ­tica','política'),'polÃ­tica','política'),'tÃ©rminos','términos')
from 
	consentimiento.dbo.consentimiento_de_paso a
	,#max_fec_cons b
where
	a.cons_rut = b.cons_rut
and a.cons_dv = b.cons_dv
and a.cons_fec_registro = b.cons_fec_registro


	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No inserta en tabla temporal #cons_new'
		return -1
	END

drop table #max_fec_cons 

/* ************************************************* */
/*actualiza registro consentimiento mas actual */
/* ************************************************* */
update consentimiento.dbo.consentimiento
set  pol_id              = b.pol_id
	,cons_rut            = b.cons_rut
	,cons_dv             = b.cons_dv
	,ori_id              = b.ori_id
	,cons_fec_registro   = b.cons_fec_registro
	,cons_ip             = b.cons_ip
	,cons_canal          = b.cons_canal
	,cons_consentimiento = b.cons_consentimiento
FROM 
	consentimiento.dbo.consentimiento a
	,#cons_new b
where
	a.cons_rut            = b.cons_rut
and a.cons_dv             = b.cons_dv
and a.cons_fec_registro < b.cons_fec_registro

	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No actualiza en tabla dbo.consentimiento'
		return -1
	END

	
/* ************************************************* */
/*tabla temporal, con los registrso nuevos de consentimiento */
/* que no existen en la tabla consentimiento*/
/* ************************************************* */
CREATE TABLE #cons_noexiste (	
	pol_id int NOT NULL
	,cons_rut int NOT NULL
	,cons_dv varchar(1) NULL
	,ori_id int NULL
	,cons_fec_registro datetime NULL
	,cons_ip varchar(15)
	,cons_canal varchar(3) NULL
	,cons_consentimiento varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	CONSTRAINT pk_cons_noexit PRIMARY KEY (cons_rut)
)

	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No crea tabla temporal temporal #cons_noexiste'
		return -1
	END
	
	
/* ************************************************* */
/*inserta a una tabla temporal, los registrso nuevos de */
/*consentimiento que no existen en la tabla consentimiento*/
/* **************************************************/	
insert into #cons_noexiste 
select
	 a.pol_id             
	,a.cons_rut           
	,a.cons_dv            
	,a.ori_id             
	,a.cons_fec_registro  
	,a.cons_ip            
	,a.cons_canal         
	,a.cons_consentimiento
from
	#cons_new a
left join
	consentimiento.dbo.consentimiento b
on a.cons_rut = b.cons_rut
and a.cons_dv = b.cons_dv
where
	b.cons_rut is null

	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No inserta en tabla temporal cons_noexiste'
		return -1
	END

drop table #cons_new 
	
	
/* ************************************************* */
/*inserta nuevo registro consentimiento */
/* ************************************************* */	
insert into consentimiento.dbo.consentimiento
select
	 pol_id             
	,cons_rut           
	,cons_dv            
	,ori_id             
	,cons_fec_registro  
	,cons_ip            
	,cons_canal         
	,cons_consentimiento
from
	#cons_noexiste

	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No inserta en tabla dbo.consentimiento'
		return -1
	END

drop table #cons_noexiste


/* ************************************************* */
/*tabla temporal con nuevos registros que ya existen en 
/ tabla consentimiento hist, para no volver a ser insertados */
/* ************************************************* */
CREATE TABLE #cons_existe_hist (	
	pol_id int NOT NULL
	,cons_rut int NOT NULL
	,cons_dv varchar(1) NULL
	,ori_id int NULL
	,cons_fec_registro datetime NULL
	,cons_ip varchar(15)
	,cons_canal varchar(3) NULL
	,cons_consentimiento varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No crea tabla temporal #cons_existe_hist'
		return -1
	END
	
	
/* ************************************************* */
/*inserta a una tabla temporal, nuevos registro consentimiento */
/*  que ya existen en la tabla consentimiento hist	*/
/* ************************************************* */	
insert into #cons_existe_hist 
select
	 a.pol_id             
	,a.cons_rut           
	,a.cons_dv            
	,a.ori_id             
	,a.cons_fec_registro  
	,a.cons_ip            
	,a.cons_canal         
	,REPLACE(REPLACE(REPLACE(a.cons_consentimiento,'polÃƒÂ­tica','política'),'polÃ­tica','política'),'tÃ©rminos','términos')
from
	consentimiento.dbo.consentimiento_de_paso a
	,consentimiento.dbo.consentimiento_historico b
where
	a.cons_rut             = b.cons_rut
and a.cons_dv 			   = b.cons_dv
and a.ori_id               = b.ori_id             
and a.cons_fec_registro    = b.cons_fec_registro  
and a.cons_ip              = b.cons_ip            
and a.cons_canal           = b.cons_canal         


	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No inserta en tabla temporal cons_existe_hist'
		return -1
	END


/* ************************************************* */
/*inserta nuevos registros en tabla consentimiento hist */
/* ************************************************* */
insert into consentimiento.dbo.consentimiento_historico
select
	 a.pol_id             
	,a.cons_rut           
	,a.cons_dv            
	,a.ori_id             
	,a.cons_fec_registro  
	,a.cons_ip            
	,a.cons_canal         
	,REPLACE(REPLACE(REPLACE(a.cons_consentimiento,'polÃƒÂ­tica','política'),'polÃ­tica','política'),'tÃ©rminos','términos')
from
	consentimiento.dbo.consentimiento_de_paso a
left join
	#cons_existe_hist b
on 	a.cons_rut             = b.cons_rut
and a.cons_dv 			   = b.cons_dv
and a.ori_id               = b.ori_id             
and a.cons_fec_registro    = b.cons_fec_registro  
and a.cons_ip              = b.cons_ip            
and a.cons_canal           = b.cons_canal  
where
	b.cons_rut is null
and b.cons_dv is null
and b.ori_id is null           
and b.cons_fec_registro is null
and b.cons_ip is null          
and b.cons_canal  is null

	
	IF @@error != 0 BEGIN
		PRINT 'Msg Error : No ejecuto --> No inserta en tabla consentimiento_historico'
		return -1
	END

drop table #cons_existe_hist

END
SET QUOTED_IDENTIFIER OFF;

go
GRANT EXECUTE ON sp_carga_migraConsentimiento TO mantencion
go
GRANT EXECUTE ON sp_carga_migraConsentimiento TO ejecucion
go 

