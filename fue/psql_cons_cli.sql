------------------------------------------------------------------------------
-- Nombre      : psql_cons_cli.sql
-- Ruta        : /MIGCON/fue/
-- Autor       : Francisco Valenzuela (Ada Ltda.) - Claudio Bello (Ing. Software. Bci)
-- Fecha       : 08/11/2021
-- Descripcion : proceso sql que extrae informacion de consentimiento de clientes desde postgres
-- Parametros  : no aplica
------------------------------------------------------------------------------
-- Mantencion  :
-- Empresa     :
-- Fecha       :
-- Descripcion :
------------------------------------------------------------------------------

COPY (

SELECT
	rut
	,dv
	,origen_id
	,fecha_registro ||' '|| '00:00:00.00000' as fec_registro
	,politica_id
	,ip
	,canal 
	,consentimiento
FROM 
	consentimientopersona.consentimientopersona
where
	fecha_registro :v1
)TO STDOUT (format CSV, delimiter ';')
