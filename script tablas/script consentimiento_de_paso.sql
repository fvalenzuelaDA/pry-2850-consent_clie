CREATE TABLE consentimiento.dbo.consentimiento_de_paso (
	cons_rut int NOT NULL,
	cons_dv varchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ori_id int NULL,
	cons_fec_registro datetime NULL,
	pol_id int NOT NULL,	
	cons_ip varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	cons_canal varchar(3) NULL,
	cons_consentimiento varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
)
go